<?php 
session_start();
include 'koneksi.php';

$username = $_SESSION['username'];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);
    if (!isset($_SESSION["login"]))
 header("location: index.php");
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>My Blog</title>
</head>

<body style="background-color:" class="mt-5">
    <style type="text/css">
        body {
            font-family: calibri;
            background-color: #d4d3d2;
        }
        .user {
            margin-top: -70px;
        }
        .footer {
           position: fixed;
           left: 0;
           bottom: 0;
           width: 100%;
           background-color: black;
           color: white;
           text-align: center;
           padding: 15px 0px 0px 10px;
           font-size: 20px;
       }
       img {
        margin-left: 500px;
        margin-top:25px;
        width: 250px;
        height: 268px;
       }
       body h2{
        font-weight: bold;
        margin-left: 30px;
        margin-top: 70px;
       }
    </style>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand " href="home.php" style="font-weight: bold;">
                <i style="font-size: 23px; color: rgb(245, 245, 245);" ></i>My Website</a>

                <div class="navbar-nav">
                    <a class="nav-link ml-3 " style="font-size:18px;  " href="home.php">Home</a>
                    <a class="nav-link active ml-3 " style="font-size:18px; " href="akun.php">User</a>
                </div>
                
                <div class="ml-auto navbar-nav">
                    <a type="button"style="width:110px;" class="btn btn-secondary" href="logout.php">Log out</a>
                </div>
        </div>
    </nav>
    <h2>Data users</h2>
    <a style="margin-left: 85px; margin-bottom: 10px;"  class="btn btn-secondary" href="registrasi.php">Tambah akun</a>
        <div class="container">
        <table class="table table-dark table-bordered">
        <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Avatar</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
             <?php
             foreach ($hasil as $key => $user_data) {
               ?>
               <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $user_data["nama"] ?></td>
                    <td><?= $user_data["username"] ?></td>
                    <td><?= $user_data["email"] ?></td>
                    <td><?= $user_data["avatar"] ?></td>
                    <td>
                        <a class="btn btn-secondary" href="form_ubah.php?id=<?=$user_data['id']?>">Ubah</a>
                        <a class="btn btn-danger" href="proses_hapus.php?id=<?=$user_data['id']?>">Hapus</a>
                    </td>
               </tr>
             <?php
            }
            ?>
            </tbody>
            <tfoot>
                
            </tfoot>
            </table>
        </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>