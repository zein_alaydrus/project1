<?php 
session_start();

if (!isset($_SESSION["login"]))
{
 header("location: index.php");exit;
}    
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        </style>
    <title>My Blog</title>
</head>

<body style="background-image: url(user.jpg);" class="mt-5">
    <style type="text/css">
        body {
            font-family: calibri;
        }
        h1 {
            margin-left: 255px;
            margin-top: -90px;
            font-family: calibri;
            font-weight: bold;
            font-size: 70px;
            color: white;
        }
        h2 {
            margin-left: 250px;
            margin-top: 250px;
            font-family: calibri;
            font-weight: bold;
            font-size: 71px;
            color: black;
        }
        .footer {
           position: fixed;
           left: 0;
           bottom: 0;
           width: 100%;
           background-color: black;
           color: white;
           text-align: center;
           padding: 15px 0px 0px 10px;
           font-size: 20px;
    </style>
    <h2>Welcome to my Website</h2>
    <h1>Welcome to my Website</h1>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand " href="home.php" style="font-weight: bold;">
                <i style="font-size: 23px; color: rgb(245, 245, 245);" ></i>My Website</a>

                <div class="navbar-nav">
                    <a class="nav-link active ml-3" style="font-size:18px; " href="home.php">Home</a>
                    <a class="nav-link ml-3" style="font-size:18px; " href="user.php">User<span class="sr-only">(current)</span></a>
                </div>
                
                <div class="ml-auto navbar-nav">
                    <a type="button"style="width:110px;" class="btn btn-secondary" href="logout.php">Log out</a>
                </div>
        </div>
    </nav>
        <div class="footer">
  
        <p>&copy; Copyright By Zein</p>

        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>