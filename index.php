<?php 
session_start();

if (isset($_GET["pesan"])){
    $pesan = $_GET["pesan"];
    } else {
    $pesan = "";
    }
if(isset($_SESSION["login"]) && ($_SESSION["login"])=== 1){
    header("location: home.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Login</title>
</head>
<style type="text/css">
    body {
        font-family: calibri;
        background-image: url(login.jpg);
    }
    body h1{
        margin-left: 170px;
    }
    .container {
        margin-top: 150px;
        padding-left: 400px;
        background: #ededed;
        padding: 30px;
        border-radius: 50px;
        box-shadow: 5px 5px 10px 5px #000;
        width: 500px;
    }
    a {
        text-decoration: none;
        font-style: italic;
    }
</style>
<body>
    <div class="container">
    <h1 style="font-weight: bold;">Login</h1>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

    <?php  echo $pesan; ?>
    
    <form method="POST" action="proses_login.php">
        <div style="margin:5px 0;">
            <label> Username : </label>
            <input style="width: 430px; border-radius: 12px; border-top-color: #ededed; border-left-color: #ededed" name="username" type="text" required> 
        </div>

        <div style="margin:5px 0;">
            <label> Password : </label><br>
            <input style="width: 430px; border-radius: 12px; border-top-color: #ededed; border-left-color: #ededed" name="password" type="password" required><br>
        </div><br>

        <button type="submit" class="btn btn-dark" style="font-weight: bold; margin-left: 40px;">ㅤㅤㅤㅤㅤㅤㅤㅤㅤLoginㅤㅤㅤㅤㅤㅤㅤㅤㅤ</button><br><br>
        belum punya akun? <a href="registrasi.php">Registrasi</a>
    </form>  
    </div>   
</body>
</html>